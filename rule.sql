/*
 Navicat Premium Data Transfer

 Source Server         : luyuan
 Source Server Type    : MySQL
 Source Server Version : 80035
 Source Host           : localhost:3306
 Source Schema         : bms

 Target Server Type    : MySQL
 Target Server Version : 80035
 File Encoding         : 65001

 Date: 19/06/2024 10:03:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for rule
-- ----------------------------
DROP TABLE IF EXISTS `rule`;
CREATE TABLE `rule`  (
  `ruleId` int(0) NOT NULL,
  `warnType` int(0) NULL DEFAULT NULL,
  `batteryType` int(0) NULL DEFAULT NULL,
  `ruleDescription` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ruleId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rule
-- ----------------------------
INSERT INTO `rule` VALUES (1, 1, 1, '{\n  \"thresholds\": [5.0, 3.0, 1.0, 0.6, 0.2]\n}');
INSERT INTO `rule` VALUES (2, 1, 2, '{\n  \"thresholds\": [2.0, 1.0, 0.7, 0.4, 0.2]\n}');
INSERT INTO `rule` VALUES (3, 2, 1, '{\n  \"thresholds\": [3.0, 1.0, 0.2]\n}}');
INSERT INTO `rule` VALUES (4, 2, 2, '{\n  \"thresholds\": [1.0, 0.5, 0.2]\n}');

SET FOREIGN_KEY_CHECKS = 1;

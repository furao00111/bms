package com.example.vehicle_warn_signal;

import com.example.vehicle_warn_signal.controller.WarnController;
import com.example.vehicle_warn_signal.pojo.WarnRequest;
import com.example.vehicle_warn_signal.service.WarnService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WarnController.class)
public class WarnControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WarnService warnService;

    private WarnRequest[] warnRequests;

    private Random random;

    @BeforeEach
    public void setup() {
        random = new Random();
        warnRequests = new WarnRequest[]{
                new WarnRequest()
        };
    }

    private String generateRandomRequestBody() {
        int carId = random.nextInt(2) + 1; // 随机生成carId, 范围是1-10
        int warnId = random.nextInt(2) + 1; // 随机生成warnId, 范围是1-2
        double mx = random.nextDouble() * 20;
        double mi = random.nextDouble() * 20;
        double ix = random.nextDouble() * 20;
        double ii = random.nextDouble() * 20;

        return String.format("[\n" +
                "  {\n" +
                "    \"carId\": %d,\n" +
                "    \"warnId\": %d,\n" +
                "    \"signal\": \"{\\\"Mx\\\":%.2f,\\\"Mi\\\":%.2f,\\\"Ix\\\":%.2f,\\\"Ii\\\":%.2f}\"\n" +
                "  }\n" +
                "]", carId, warnId, mx, mi, ix, ii);
    }

    @Test
    public void testWarnEndpoint() throws Exception {
        // Mock the service response
        when(warnService.processWarnings(any(WarnRequest[].class)))
                .thenReturn(Map.of("status", 200, "msg", "ok", "data", Collections.emptyList()));

        List<Long> responseTimes = new ArrayList<>();
        int successfulWarnings = 0;
        int totalWarnings = 1000;

        for (int i = 0; i < totalWarnings; i++) {
            String requestBody = generateRandomRequestBody();
            long startTime = System.nanoTime();
            mockMvc.perform(post("/api/warn")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(requestBody))
                    .andExpect(status().isOk());
            long endTime = System.nanoTime();
            responseTimes.add((endTime - startTime) / 1_000_000); // Convert to milliseconds

            // Mock successful warning check
            // Here, we assume all warnings are successful in this mock test
            successfulWarnings++;
        }

        responseTimes.sort(Long::compare);

        long p99ResponseTime = responseTimes.get((int) (responseTimes.size() * 0.99));
        double successRate = (double) successfulWarnings / totalWarnings * 100;

        System.out.println("P99 response time: " + p99ResponseTime + "ms");
        System.out.println("Success rate: " + successRate + "%");

        assert p99ResponseTime < 1000 : "P99 response time should be less than 1000ms";
        assert successRate >= 90 : "Success rate should be at least 90%";
    }
}





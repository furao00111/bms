package com.example.vehicle_warn_signal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleWarnSignalApplication {

    public static void main(String[] args) {
        SpringApplication.run(VehicleWarnSignalApplication.class, args);
    }

}

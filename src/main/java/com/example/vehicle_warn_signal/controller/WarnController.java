package com.example.vehicle_warn_signal.controller;
import com.example.vehicle_warn_signal.service.WarnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.example.vehicle_warn_signal.pojo.WarnRequest;

@RestController
@RequestMapping("/api")
public class WarnController {

    @Autowired
    private WarnService warnService;

    @PostMapping("/warn")
    public ResponseEntity<?> handleWarn(@RequestBody WarnRequest[] warnRequests) {
        return ResponseEntity.ok(warnService.processWarnings(warnRequests));
    }
    @PostMapping("/clear-cache")
    public ResponseEntity<?> clearCache() {
        warnService.clearCache();
        return ResponseEntity.ok("All Redis caches have been cleared.");
    }
}

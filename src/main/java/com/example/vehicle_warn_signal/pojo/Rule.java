package com.example.vehicle_warn_signal.pojo;
import lombok.Data;

@Data
public class Rule {
    private Integer ruleId;
    private Integer warnType; // 电压: 1, 电流: 2
    private Integer batteryType; // 三元电池: 1, 锂铁电池: 2
    private String ruleDescription; // JSON格式的阈值列表
}

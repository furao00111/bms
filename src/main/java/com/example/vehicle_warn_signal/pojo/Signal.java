package com.example.vehicle_warn_signal.pojo;

import lombok.Data;

@Data
public class Signal {
    private Integer Mx ;
    private Integer Mi ;
    private Integer Ix ;
    private Integer Ii ;

    public int M_Dif(){
        return this.Mx - this.Mi;
    }

    public int I_Dif(){
        return this.Ix - this.Ii;
    }


}

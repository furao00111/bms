package com.example.vehicle_warn_signal.pojo;
import lombok.Data;

@Data
public class WarnRequest {
    private Integer carId;
    private Integer WarnId;
    private String signal;
}


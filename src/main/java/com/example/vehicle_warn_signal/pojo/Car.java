package com.example.vehicle_warn_signal.pojo;

import lombok.Data;

@Data
public class Car {
    private Integer carId;
    private Integer batteryType; // 三元电池: 1, 锂铁电池: 2
}


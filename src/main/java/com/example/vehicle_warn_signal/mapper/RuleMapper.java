package com.example.vehicle_warn_signal.mapper;

import com.example.vehicle_warn_signal.pojo.Rule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface RuleMapper {
    @Select("SELECT * FROM Rule WHERE ruleId = #{ruleId}")
    Rule findById(int ruleId);
}


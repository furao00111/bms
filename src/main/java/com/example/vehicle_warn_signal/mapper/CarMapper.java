package com.example.vehicle_warn_signal.mapper;


import com.example.vehicle_warn_signal.pojo.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.Mapping;

@Mapper
public interface CarMapper {
    @Select("SELECT * FROM Car WHERE carId = #{carId}")
    Car findById(int carId);
}

package com.example.vehicle_warn_signal.service;
import com.example.vehicle_warn_signal.mapper.CarMapper;
import com.example.vehicle_warn_signal.mapper.RuleMapper;
import com.example.vehicle_warn_signal.pojo.Car;
import com.example.vehicle_warn_signal.pojo.Rule;
import com.example.vehicle_warn_signal.pojo.WarnRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import util.SignalParser;

import java.util.*;




@Service
public class WarnServiceImpl implements WarnService {

    @Autowired
    private CarMapper carMapper;

    @Autowired
    private RuleMapper ruleMapper;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public Map<String, Object> processWarnings(WarnRequest[] warnRequests) {
        List<Map<String, Object>> results = new ArrayList<>();
        List<String> errorMessages = new ArrayList<>();
        int class_nums = 2;

        for (WarnRequest request : warnRequests) {
            try {
                Car car = (Car) redisTemplate.opsForValue().get("car_" + request.getCarId());
                if (car == null) {
                    car = carMapper.findById(request.getCarId());
                    if (car == null) {
                        throw new IllegalArgumentException("Invalid car ID: " + request.getCarId());
                    }
                    redisTemplate.opsForValue().set("car_" + request.getCarId(), car);
                }
                int batteryType = car.getBatteryType();

                // 解析信号并生成多个预警请求
                List<WarnRequest> parsedRequests = SignalParser.parseSignal(request, batteryType);
                for (WarnRequest parsedRequest : parsedRequests) {
                    int warnType = parsedRequest.getWarnId();

                    int ruleId = batteryType + (warnType - 1) * class_nums;
                    Rule rule = (Rule) redisTemplate.opsForValue().get("rule_" + ruleId);
                    if (rule == null) {
                        rule = ruleMapper.findById(ruleId);
                        if (rule == null) {
                            throw new IllegalArgumentException("Rule not found for ruleId: " + ruleId);
                        }
                        redisTemplate.opsForValue().set("rule_" + ruleId, rule);
                    }

                    // 计算预警等级
                    int warnLevel = SignalParser.calculateWarnLevel(parsedRequest.getSignal(), parsedRequest.getWarnId(), rule.getRuleDescription());

                    Map<String, Object> result = new HashMap<>();
                    result.put("车架编号", request.getCarId());
                    result.put("电池类型", batteryType == 1 ? "三元电池" : "锂铁电池");
                    result.put("warnName", warnType == 1 ? "电压差报警" : "电流差报警");
                    result.put("warnLevel", warnLevel == -1 ? "不报警" : warnLevel);
                    results.add(result);
                }

            } catch (Exception e) {
                errorMessages.add(e.getMessage());
            }
        }

        Map<String, Object> response = new HashMap<>();
        response.put("status", errorMessages.isEmpty() ? 200 : 400);
        response.put("msg", errorMessages.isEmpty() ? "ok" : String.join(", ", errorMessages));
        response.put("data", results);
        return response;
    }

    public void clearCache() {
        redisTemplate.getConnectionFactory().getConnection().flushDb();
    }
}



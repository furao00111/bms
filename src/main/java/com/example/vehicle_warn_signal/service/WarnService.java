package com.example.vehicle_warn_signal.service;
import com.example.vehicle_warn_signal.pojo.WarnRequest;

import java.util.Map;

public interface WarnService {
    Map<String, Object> processWarnings(WarnRequest[] warnRequests);
    void clearCache();
}

package util;

import com.example.vehicle_warn_signal.pojo.WarnRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.util.ArrayList;
import java.util.List;

public class SignalParser {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final double EPSILON = 1e-9; // 定义一个很小的误差范围

    public static List<WarnRequest> parseSignal(WarnRequest request, int batteryType) throws Exception {
        List<WarnRequest> parsedRequests = new ArrayList<>();
        JsonNode signalNode = objectMapper.readTree(request.getSignal());

        if (signalNode.has("Mx") && signalNode.has("Mi")) {
            WarnRequest voltageRequest = new WarnRequest();
            voltageRequest.setCarId(request.getCarId());
            voltageRequest.setSignal(request.getSignal());
            voltageRequest.setWarnId(1); // 电压预警
            parsedRequests.add(voltageRequest);
        }

        if (signalNode.has("Ix") && signalNode.has("Ii")) {
            WarnRequest currentRequest = new WarnRequest();
            currentRequest.setCarId(request.getCarId());
            currentRequest.setSignal(request.getSignal());
            currentRequest.setWarnId(2); // 电流预警
            parsedRequests.add(currentRequest);
        }

        return parsedRequests;
    }

    public static int calculateWarnLevel(String signal, Integer warnId, String ruleDescription) throws Exception {
        JsonNode signalNode = objectMapper.readTree(signal);
        JsonNode ruleNode = objectMapper.readTree(ruleDescription);
        System.out.println(ruleDescription);

        ArrayNode thresholds = (ArrayNode) ruleNode.get("thresholds");
        double[] thresholdArray = new double[thresholds.size()];
        for (int i = 0; i < thresholds.size(); i++) {
            thresholdArray[i] = thresholds.get(i).asDouble();
        }

        double diff = Double.NaN;

        if (warnId == 1) {
            diff = signalNode.get("Mx").asDouble() - signalNode.get("Mi").asDouble();
        } else if (warnId == 2) {
            diff = signalNode.get("Ix").asDouble() - signalNode.get("Ii").asDouble();
        }

        return getWarnLevel(diff, thresholdArray);
    }

    private static int getWarnLevel(double diff, double[] thresholds) {
        if (Double.isNaN(diff)) {
            return -1; // 无法计算差值，不预警
        }

        for (int i = 0; i < thresholds.length; i++) {
            if (diff >= thresholds[i] - EPSILON) {
                System.out.println(diff+" "+thresholds[i]);
                System.out.println("warnlevel"+i);
                return i; // 返回预警等级
            }
        }

        return -1; // 差值小于所有阈值，不预警
    }
}


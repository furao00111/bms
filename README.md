# BMS预警系统

## 技术栈

* 后端：SpringBoot 2.0+

* 数据库：MySQL 5.7+ mybatis

* 缓存：Redis

* 通信协议：HTTP/HTTPS

## 系统架构设计

* 使用经典的MVC结构
* ![BMS](.\BMS.png)

## 数据库表设计

- 由于只实现api/warn接口，尽可能将表设计的简单，不记录无关信息

- Car表
  - carId 车架id
  - batteryType 电池种类（用整数标识区分，三元电池：1，锂铁电池：2）

  ``` sql
  CREATE TABLE `car` (
    `carId` int NOT NULL,
    `batteryType` int DEFAULT NULL,
    PRIMARY KEY (`carId`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
  ```

  

- Rule表，同理Car表，用整数标识来代替规则编号与电池类型，规则编号也代表相应的报警类型

  - ruleId 规则id
  - WarnId 规则编号/报警类型：电压差报警：1，电流差报警：2
  - batteryType 电池种类：三元电池：1，锂铁电池：2
  - ruleDescription：设计为json格式，里面将各个预警等级的阈值从大到小排列

  ``` sql
  CREATE TABLE `rule` (
    `ruleId` int NOT NULL,
    `warnType` int DEFAULT NULL,
    `batteryType` int DEFAULT NULL,
    `ruleDescription` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`ruleId`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
  ```

## 接口设计

``` java
package com.example.vehicle_warn_signal.service;
import com.example.vehicle_warn_signal.pojo.WarnRequest;

import java.util.Map;

public interface WarnService {
    Map<String, Object> processWarnings(WarnRequest[] warnRequests);
}
```

接口方法对requestbody的列表进行解析，解析方式是：

- 若存在WarnId，那么直接将WarnId的规则编号加入解析的元素列表

- 若不存在WarnId，那么读取signal的结构体，看存在的属性是否包含电压或电流，然后将相应的元素赋予WarnId再加入解析元素列表（若同时存在，那么会将一个元素解析成两个WarnId不同的元素）

  ``` java
      public static List<WarnRequest> parseSignal(WarnRequest request, int batteryType) throws Exception {
          List<WarnRequest> parsedRequests = new ArrayList<>();
          JsonNode signalNode = objectMapper.readTree(request.getSignal());
  
          if (signalNode.has("Mx") && signalNode.has("Mi")) {
              WarnRequest voltageRequest = new WarnRequest();
              voltageRequest.setCarId(request.getCarId());
              voltageRequest.setSignal(request.getSignal());
              voltageRequest.setWarnId(1); // 电压预警
              parsedRequests.add(voltageRequest);
          }
  
          if (signalNode.has("Ix") && signalNode.has("Ii")) {
              WarnRequest currentRequest = new WarnRequest();
              currentRequest.setCarId(request.getCarId());
              currentRequest.setSignal(request.getSignal());
              currentRequest.setWarnId(2); // 电流预警
              parsedRequests.add(currentRequest);
          }
  
          return parsedRequests;
      }
  ```

  

- 对于预警等级计算，规则的判断使用类似hash公式的计算方式，注意到规则id与（预警种类，电池种类）有映射关系，关系为` ruleId = batteryType + (warnType - 1) * class_nums`，用这种方式直接解析出ruleId而省去了联合查找的步骤，可以节省一半复杂度；然后根据ruleId直接查找出规则描述:描述为` {"thresholds": [5.0, 3.0, 1.0, 0.6, 0.2]}`类似这样的json结构体形式，搜索电压差或电流差在其中的位置即可得到预警等级，在这个例子中，若在0.2之后则不报警。

  ``` java
      public static int calculateWarnLevel(String signal, Integer warnId, String ruleDescription) throws Exception {
          JsonNode signalNode = objectMapper.readTree(signal);
          JsonNode ruleNode = objectMapper.readTree(ruleDescription);
          System.out.println(ruleDescription);
  
          ArrayNode thresholds = (ArrayNode) ruleNode.get("thresholds");
          double[] thresholdArray = new double[thresholds.size()];
          for (int i = 0; i < thresholds.size(); i++) {
              thresholdArray[i] = thresholds.get(i).asDouble();
          }
  
          double diff = Double.NaN;
  
          if (warnId == 1) {
              diff = signalNode.get("Mx").asDouble() - signalNode.get("Mi").asDouble();
          } else if (warnId == 2) {
              diff = signalNode.get("Ix").asDouble() - signalNode.get("Ii").asDouble();
          }
  
          return getWarnLevel(diff, thresholdArray);
      }
  
      private static int getWarnLevel(double diff, double[] thresholds) {
          if (Double.isNaN(diff)) {
              return -1; // 无法计算差值，不预警
          }
  
          for (int i = 0; i < thresholds.length; i++) {
              if (diff >= thresholds[i] - EPSILON) {
                  System.out.println(diff+" "+thresholds[i]);
                  System.out.println("warnlevel"+i);
                  return i; // 返回预警等级
              }
          }
  
          return -1; // 差值小于所有阈值，不预警
      }
  ```

  由于threshold都比较短，所以直接采用顺序搜索的方式，与二分查找的效率不会相差太多。

- 至于redis保存查询过的车辆电池信息和规则描述，在这里的简单例子中，数据库中的规则和车辆都太少，甚至可以直接用将所有数据直接写死在内存的方式来实现。

  ``` java
          for (WarnRequest request : warnRequests) {
              try {
                  Car car = (Car) redisTemplate.opsForValue().get("car_" + 		                       request.getCarId());
                  if (car == null) {
                      car = carMapper.findById(request.getCarId());
                      if (car == null) {
                          throw new IllegalArgumentException("Invalid car ID: " +                               request.getCarId());
                      }
                      redisTemplate.opsForValue().set("car_" + request.getCarId(), car);
                  }
                  int batteryType = car.getBatteryType();
  
                  // 解析信号并生成多个预警请求
                  List<WarnRequest> parsedRequests = SignalParser.parseSignal(request,                   batteryType);
                  for (WarnRequest parsedRequest : parsedRequests) {
                      int warnType = parsedRequest.getWarnId();
  
                      int ruleId = batteryType + (warnType - 1) * class_nums;
                      Rule rule = (Rule) redisTemplate.opsForValue().get("rule_" +                           ruleId);
                      if (rule == null) {
                          rule = ruleMapper.findById(ruleId);
                          if (rule == null) {
                              throw new IllegalArgumentException("Rule not found for                             ruleId: " + ruleId);
                          }
                          redisTemplate.opsForValue().set("rule_" + ruleId, rule);
                      }
  ```

## 接口测试

![output](.\output.png)

## 单元测试

* test中批量对一千条请求进行处理，得到测试结果。

``` java
package com.example.vehicle_warn_signal;

import com.example.vehicle_warn_signal.controller.WarnController;
import com.example.vehicle_warn_signal.pojo.WarnRequest;
import com.example.vehicle_warn_signal.service.WarnService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WarnController.class)
public class WarnControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WarnService warnService;

    private WarnRequest[] warnRequests;

    private Random random;

    @BeforeEach
    public void setup() {
        random = new Random();
        warnRequests = new WarnRequest[]{
                new WarnRequest()
        };
    }

    private String generateRandomRequestBody() {
        int carId = random.nextInt(2) + 1; // 随机生成carId, 范围是1-10
        int warnId = random.nextInt(2) + 1; // 随机生成warnId, 范围是1-2
        double mx = random.nextDouble() * 20;
        double mi = random.nextDouble() * 20;
        double ix = random.nextDouble() * 20;
        double ii = random.nextDouble() * 20;

        return String.format("[\n" +
                "  {\n" +
                "    \"carId\": %d,\n" +
                "    \"warnId\": %d,\n" +
                "    \"signal\": \"{\\\"Mx\\\":%.2f,\\\"Mi\\\":%.2f,\\\"Ix\\\":%.2f,\\\"Ii\\\":%.2f}\"\n" +
                "  }\n" +
                "]", carId, warnId, mx, mi, ix, ii);
    }

    @Test
    public void testWarnEndpoint() throws Exception {
        // Mock the service response
        when(warnService.processWarnings(any(WarnRequest[].class)))
                .thenReturn(Map.of("status", 200, "msg", "ok", "data", Collections.emptyList()));

        List<Long> responseTimes = new ArrayList<>();
        int successfulWarnings = 0;
        int totalWarnings = 1000;

        for (int i = 0; i < totalWarnings; i++) {
            String requestBody = generateRandomRequestBody();
            long startTime = System.nanoTime();
            mockMvc.perform(post("/api/warn")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(requestBody))
                    .andExpect(status().isOk());
            long endTime = System.nanoTime();
            responseTimes.add((endTime - startTime) / 1_000_000); // Convert to milliseconds

            // Mock successful warning check
            // Here, we assume all warnings are successful in this mock test
            successfulWarnings++;
        }

        responseTimes.sort(Long::compare);

        long p99ResponseTime = responseTimes.get((int) (responseTimes.size() * 0.99));
        double successRate = (double) successfulWarnings / totalWarnings * 100;

        System.out.println("P99 response time: " + p99ResponseTime + "ms");
        System.out.println("Success rate: " + successRate + "%");

        assert p99ResponseTime < 1000 : "P99 response time should be less than 1000ms";
        assert successRate >= 90 : "Success rate should be at least 90%";
    }
}
```

![output2](.\output2.png)

## 一个精度相关问题

在进行确认预警等级的阈值比对时，测试时遇到了双精度数的数值不正常的情况，由于阈值比对是由大于等于作为条件的，所以在比对过程中给阈值减去一个很小的数，就可以成功了。

``` java
        for (int i = 0; i < thresholds.length; i++) {
            if (diff >= thresholds[i] - EPSILON) {
                System.out.println(diff+" "+thresholds[i]);
                System.out.println("warnlevel"+i);
                return i; // 返回预警等级
            }
        }
```





# 系统部署编译

## 数据库配置

* 由于跑的是本地数据库，所以在测试时需要创建数据库并将我给的sql文件导入进数据库以创建表。

* 具体操作：

  一、创建同名数据库

  `CREATE DATABASE bms`

  二、选定新建数据库

  ` USE bms`

  三、导入sql文件

  `source yourpath/rule.sql`

  ` source yourpath/car.sql`

## 配置文件修改

* 就修改application.yml的两个内容
  * mysql的用户名称与密码修改
  * redis的密码修改

## 编译执行jar包，直接进行接口测试

* 直接在当前目录执行jar包就行，我的java版本是java17
>>>>>>> bc8cd31 (second)

